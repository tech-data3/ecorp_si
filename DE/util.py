import unidecode
import Levenshtein
import math

def normalize_str(string : str) -> str:
    string = unidecode.unidecode(string.strip().upper())
    return string
def are_similar_word(word1: str, word2: str) -> bool:
    """ return true if words are close enough using logarithm of the length of the largest word"""
    if word1 != word2 and Levenshtein.distance(word1, word2) <= (val_max := int(math.log(max(len(word1), len(word2))))):
        return True
    return False

def rename_product_using_similar_word(series):
    for i, name1 in enumerate(series):
        for j, name2 in enumerate(series[i+1:]):
            if are_similar_word(name1, name2):
                series[j+i+1] = name1
    return series
