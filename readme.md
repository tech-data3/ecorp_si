# ECF ECORP SI

## data exploration
dans aucun des fichier il n'y a d'unite. on ne sait pas si on a a faire a des pots a des l a des kg
Le client n'est joue par personne du coup il faudra prendre des decisions.

Toutes les valeurs qui semblent representer des quantitees sont des entiers dans les 3 fichier
Il n'y a aucune valeur nulles dans aucun des 3 fichiers
Il faut prendre une decision concernant les unitees utilisees. elle semble etre differente entre les fichiers

### what we have mld (like)
![what we have mld](docs/what_we_have_mld.png)
### extract_A.csv
#### first reading
deux colonnes product, stock

on deduit que stock represente une quantite et que product est le nom du produit stocke
Au premier regard il semble il y avoir des doublons (exemple cyanure).
les noms des produits ne sont pas normalises la casse est differente pour chacun.

#### data exploration summary

Il n'y a pas de valeurs null dans le tableau et la colonne stock est bien une colone d'entier
il semble difficile de determiner quel unite est represente par quel quantite. il faudra faire
des choix.
apres avoir mis tout les noms de produit en majuscules on se rend compte que oeuf est ecris
de deux maniere differentes. avec s et sans S
les autres noms de produits semble etre normaux

#### interpretation du fichier

les valeurs representent la quantitees varie de 3 a 117 
il y a des doublons de noms de produit.
On pourrait imaginer que ce soit des donnees d'inventaire dans ce cas la le dernier doublons est
celui qui compte en partant du principe qu'une nouvelle ligne d'inventaire est toujours ajouter en bas
du fichier lorsqu'il y a un ajout.
Le sujet est un restaurant 
par exemple la valeur 109 pour pain, representent elle 109 baguette, 109 grammes, 109 kilo
creme epaisse par exemple 3 represente le nombre de pot ? 3 kilo ou 3 gramme.
peut on imaginer definir 
Il s'agit de restaurant, je ne me rend pas bien compte de la quantite des strocks d'un restaurant 
a un instant T


### extract_B.csv
#### first reading
trois colonnes stock,date,inventory
ici la colonne stock represente le nom du produit, la date est une date, et enfin la colonne inventory semble etre la quantite de produit stoke

la date semble etre au format fr jour/mois/annees, cependant il est difficile de savoir a quoi elle correspond
est ce la date d'inventaire, 
Il y a des doublons dans la colones des noms cependant le fait qu'il y ait une date (qui pourrait etre la date d'inventaire)
semble faire en sorte qu'il ne s'agisse pas de doublons mais de l'etat des stocks a une date donnees.

#### data exploration summary
Il n'y a pas de valeurs nul dans l'echantillon

les unites des quantitees semblent differentes du fichier extract_A en effet les valeurs varie 
entre 0 et 27. 

De meme que dans le fichier A un meme nom de produit peut etre ecris differement (casse differentes, ainsi que pluriel pour oeuf a nouveau)

la dispersion des dates semble homogenes il ne semble pas qu'il y ait de dates avec un format different
## extract_C.csv
### first reading
2 colonnes non nommees

la premiere colonne represente le nom du produit et la derniere colonne semble etre l'inventaire.
En realite il y a beaucoup de nombre negatif a linterieur de celle ci. a premiere vu on peut 
imaginer qu'il s'agit de la variation du stock. si il est ecrit -10 peut etre peut on dire
que 10 ont ete utilise.
A premiere vu il semble difficile de connaitre la realite du stock a un instant T
si nous prennons le cas du sel dans ce fichier. il n'y a que 2 chiffres negatif. disons qu'il a ete utilise
deux fois du sel. on ne peut pas dire si il en reste ou pas.

Il est dur de trouver une logique pour ce fichier en particulier en effet des fois il y a des 0 si
cela etait le moment de rajouter ou d'enlever du stock pourquoi enregistrer le fait d'avoir enleve ou rajoute 0

#### data exploration summary

ce fichier n'a pas de nom de colonne

les valeurs pour celui ci varie de -50 a 50. il n'y a pas de valeurs nuls

meme traitement que pour les autres fichiers

ici pas de piege avec oeuf

#### interpretation du fichier

Les valeurs representant les quantites ici sont positives et negatives. Il me semble que 
cela representent des variations.
du coup la valeur du stock pour un produit correspond a la somme des stocks passees pour un produit
donnees.
Je prendrai en compte la valeur final comme etant letat du stock a l'instant t.



