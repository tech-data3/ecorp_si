import pandas as pd
from src.api import util

df = pd.read_csv('../data/recettes.csv')
print(df)

df['ingredient'] = df['ingredient'].str.lower().str.strip()
df['label_i'] = df['ingredient'].apply(util.normalize_str)
df['name'] = df['name'].str.lower().str.strip()
df['label_n'] = df['name'].apply(util.normalize_str)
print(df)
util.recipe_dataframe_to_sql(df)