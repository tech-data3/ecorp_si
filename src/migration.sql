USE ecorp_si;

CREATE TABLE recipe(
    recipe_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50),
    label VARCHAR(50),
    UNIQUE (name),
    UNIQUE (label)
);

CREATE TABLE product_recipe(
    product_id INT NOT NULL REFERENCES product.product_id,
    recipe_id INT NOT NULL REFERENCES recipe.recipe_id,
    qt INT NOT NULL,
    PRIMARY KEY(product_id, recipe_id)
);

