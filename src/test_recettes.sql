USE ecorp_si;

SELECT
    recipe_id
FROM
    recipe
WHERE
    name = 'crepe';
-- LA RECETTE DES CREPES N'EXISTE PAS DEJA SUPER JE PEUX LA CREER

SELECT
    product_id, name
FROM
    product
WHERE
    label IN ('OEUF','FARINE','SUCRE','LAIT','INGREDIENT SECRET');
-- OH ! MINCE ALORS IL N'Y A PAS DE SUCRE ET D'INGREDIENT SECRET
-- DEVRAIS JE LES CREER ? JE ME POSE LA QUESION

INSERT INTO
    product(name, label)
VALUES
    ('sucre', 'SUCRE'),
    ('ingrédients secrets','INGREDIENT SECRET');

SET @oeuf = (
SELECT
    product_id
FROM
    product
WHERE
    label = 'OEUF'
);

SET @farine = (
SELECT
    product_id
FROM
    product
WHERE
    label = 'FARINE'
);

SET @sucre = (
SELECT
    product_id
FROM
    product
WHERE
    label = 'SUCRE'
);
SET @lait = (
SELECT
    product_id
FROM
    product
WHERE
    label = 'LAIT'
);
SET @ingredient_secret = (
SELECT
    product_id
FROM
    product
WHERE
    label = 'INGREDIENT SECRET'
);


-- J'AI TOUT CE QU'IL FAUT POUR FAIRE DES CREPEES
INSERT INTO
    recipe(name, label)
VALUES
    ('crepes', 'CREPE');

SET @recipe = (SELECT LAST_INSERT_ID());
-- ET MAINTENANT JE REMPLI LA TABLE DE RELATION
INSERT INTO
    product_recipe(product_id, recipe_id, qt)
VALUES
    (@farine,@recipe,100),
    (@ingredient_secret,@recipe,1),
    (@lait,@recipe,100),
    (@oeuf,@recipe,1),
    (@sucre,@recipe,100);






