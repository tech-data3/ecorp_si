import pandas as pd
import unidecode
import Levenshtein
import math

from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session

import src.api.bdd_access
import src.api.bdd_model


def normalize_str(string: str) -> str:
    return " ".join(
        [unidecode.unidecode(''.join([c for c in (mot[:-1] if mot[-1] == 'S' else mot) if c.isalnum() or c == '-'])) for mot in string.strip().upper().split()]
    )


def are_similar_word(word1: str, word2: str) -> bool:
    """ return true if words are close enough using logarithm of the length of the largest word"""
    if word1 != word2 and Levenshtein.distance(word1, word2) <= (val_max := int(math.log(max(len(word1), len(word2))))):
        return True
    return False


def rename_product_using_similar_word(series):
    for i, name1 in enumerate(series):
        for j, name2 in enumerate(series[i + 1:]):
            if are_similar_word(name1, name2):
                series[j + i + 1] = name1
    return series


def dataframe_to_sql(df: pd.DataFrame):
    r = src.api.bdd_model.Restaurant()
    with Session(src.api.bdd_access.engine).no_autoflush as session:
        for i, item in df.iterrows():
            product_dict = item.to_dict()
            s = src.api.bdd_model.Stock(qt=product_dict['qt'], inventory_date=product_dict['date'])
            for _product in r.products:
                if _product.product.label == product_dict['label']:
                    product = _product.product
                    break
            else:
                try:
                    product = session.scalars(
                        select(src.api.bdd_model.Product)
                        .where(src.api.bdd_model.Product.label == product_dict['label'])
                    ).one()
                except NoResultFound:
                    if not isinstance(product_dict['name'], str):
                        product_dict['name'] = next(iter(product_dict['name']))
                    product = src.api.bdd_model.Product(
                        name=product_dict['name'],
                        label=product_dict['label']
                    )
            s.product = product
            r.products.append(s)
        session.add(r)
        session.commit()

def recipe_dataframe_to_sql(df: pd.DataFrame):
    recipes = []
    products = []
    with Session(src.api.bdd_access.engine).no_autoflush as session:
        for i, row in df.iterrows():
            product_recipe_from_df = row.to_dict()
            for recipe in recipes:
                if recipe.label == product_recipe_from_df['label_n']:
                    break
            else:
                try:
                    recipe = session.scalars(
                        select(src.api.bdd_model.Recipe)
                        .where(src.api.bdd_model.Recipe.label == product_recipe_from_df['label_n'])
                    ).one()
                except NoResultFound:
                    recipe = src.api.bdd_model.Recipe(
                        name = product_recipe_from_df['name'],
                        label = product_recipe_from_df['label_n']
                    )
                    recipes.append(recipe)
            for product in products:
                if product.label == product_recipe_from_df['label_i']:
                    break
            else:
                try:
                    product = session.scalars(
                        select(src.api.bdd_model.Product)
                        .where(src.api.bdd_model.Product.label == product_recipe_from_df['label_i'])
                    ).one()
                except NoResultFound:
                    product = src.api.bdd_model.Product(
                        name = product_recipe_from_df['ingredient'],
                        label = product_recipe_from_df['label_i']
                    )
                    products.append(product)
            product_recipe = src.api.bdd_model.Product_Recipe(qt= product_recipe_from_df['spoon'])
            product_recipe.product = product
            session.add(product_recipe)
            recipe.products.append(product_recipe)
        session.add_all(recipes)
        session.commit()



if __name__ == '__main__':
    mots = ['oeufs', 'oeuf', 'fruits confits melanges']
    for mot in mots:
        print(normalize_str(mot))