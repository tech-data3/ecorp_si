from pprint import pprint

import fastapi
from fastapi import FastAPI, HTTPException
from sqlalchemy.exc import NoResultFound

import src.api.bdd_model
import src.api.dto_model
import src.api.util

app = FastAPI()


@app.get("/product/{name}")
def get_product(name: str) -> src.api.dto_model.StockOut:
    result = src.api.bdd_model.execute_select_product_qty_per_restaurant(
        src.api.util.normalize_str(name)
    )
    result['stocks']=list(map(src.api.bdd_model.serialize, result['stocks']))
    if not result['stocks']:
        raise HTTPException(status_code=404, detail='item not found')

    return src.api.dto_model.StockOut(
        **{
            'product' : src.api.bdd_model.serialize(result['product']),
            'stock': result['stocks'],
            'cumul': sum(s['qt'] for s in result['stocks'])
        }
    )
@app.post("/product")
def create_product(product: src.api.dto_model.ProductIn) -> src.api.dto_model.ProductOut:
    product = product.model_dump()
    product['label'] = src.api.util.normalize_str(product['name'])
    return src.api.dto_model.ProductOut(
            **src.api.bdd_model.execute_insert_product(**product).__dict__
        )

@app.put("/stock")
def update_qty_product(stock: src.api.dto_model.StockLineIn):
    try:
        result = src.api.bdd_model.execute_update_stock(**stock.model_dump())
    except ValueError:
        raise HTTPException(
            status_code=400,
            detail="Le stock n'a pas ete mis a jour. Vous tentez d'enlever plus que vous n'avez"
        )
    except NoResultFound:
        raise HTTPException(status_code=404, detail="item not found")
    return src.api.dto_model.StockLineOut(
        **src.api.bdd_model.serialize(result)
    )

@app.post("/recipe")
def create_recipe(recipe: src.api.dto_model.RecipeIn) -> src.api.dto_model.RecipeOut:
    recipe = recipe.model_dump()
    insert_recipe = src.api.bdd_model.execute_insert_recipe(**recipe)
    serialized_recipe = src.api.bdd_model.serialize(insert_recipe)
    print('main 58',serialized_recipe)
    return src.api.dto_model.RecipeOut(**serialized_recipe)

@app.get('/recipe/{name}')
def read_recipe_by_name(name: str) -> src.api.dto_model.RecipeOut:
    recipe = src.api.bdd_model.execute_select_recipe_by_name(src.api.util.normalize_str(name))
    if not recipe:
        raise HTTPException(status_code=404, detail="item not found")
    return src.api.dto_model.RecipeOut(
        **src.api.bdd_model.serialize(recipe)
    )

@app.get('/recipe/restaurant/{name}')
def read_recipe_by_restaurant(name: str) -> src.api.dto_model.QtRecipeByRestaurant:
    recipe_by_restaurant = src.api.bdd_model.execute_select_recipe_qt_by_restaurant(name)
    if not recipe_by_restaurant:
        raise HTTPException(status_code=404, detail='item not found')
    return src.api.dto_model.QtRecipeByRestaurant(
        **recipe_by_restaurant
    )
