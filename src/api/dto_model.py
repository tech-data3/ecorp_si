import datetime
from typing import Optional, List

from pydantic import BaseModel

class StockLine(BaseModel):
    restaurant_id: int
    qt: int

class StockLineIn(StockLine):
    product_id: int


class ProductIn(BaseModel):
    name: str

class ProductOut(ProductIn):
    product_id: int
    label: str

class StockOut(BaseModel):
    product: ProductOut
    stock: Optional[List[StockLine]]
    cumul: int
class RestaurantOut(BaseModel):
    restaurant_id: int
class StockLineOut(BaseModel):
    product: ProductOut
    restaurant: RestaurantOut
    qt: int
    inventory_date: datetime.date
class ProductRecipeIn(BaseModel):
    product: ProductIn
    qt: int
class RecipeIn(BaseModel):
    name: str
    products: List[ProductRecipeIn]

class ProductRecipeOut(BaseModel):
    product_id: int
    product: ProductOut
    qt: int
class RecipeOut(RecipeIn):
    recipe_id: int
    label: str
    products: List[ProductRecipeOut]

class QtRecipe(BaseModel):
    restaurant: RestaurantOut
    qt: int

class QtRecipeByRestaurant(BaseModel):
    recipe: RecipeOut
    qt: List[QtRecipe]