import datetime
from typing import List
from sqlalchemy import String, select, ForeignKey
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlalchemy.orm import Session, relationship, mapped_column, Mapped, selectinload

import src.api.util
from src.api.bdd_access import engine, Base


class Stock(Base):
    __tablename__ = 'product_restaurant'
    id: Mapped[int] = mapped_column(primary_key=True)
    product_id: Mapped[int] = mapped_column(ForeignKey('product.product_id'))
    restaurant_id: Mapped[int] = mapped_column(ForeignKey('restaurant.restaurant_id'))
    qt: Mapped[int]
    inventory_date: Mapped[datetime.date]
    product: Mapped["Product"] = relationship(back_populates="restaurants")
    restaurant: Mapped["Restaurant"] = relationship(back_populates="products")


class Restaurant(Base):
    __tablename__ = 'restaurant'
    restaurant_id: Mapped[int] = mapped_column(primary_key=True)
    products: Mapped[List["Stock"]] = relationship(back_populates="restaurant")


class Product(Base):
    __tablename__ = "product"
    product_id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(50))
    label: Mapped[str] = mapped_column(String(50))
    restaurants: Mapped[List["Stock"]] = relationship(back_populates="product")


class Product_Recipe(Base):
    __tablename__ = 'product_recipe'
    product_id: Mapped[int] = mapped_column(ForeignKey('product.product_id'), primary_key=True)
    recipe_id: Mapped[int] = mapped_column(ForeignKey('recipe.recipe_id'), primary_key=True)
    qt: Mapped[int]
    recipe: Mapped['Recipe'] = relationship(back_populates='products')
    product: Mapped['Product'] = relationship()


class Recipe(Base):
    __tablename__ = "recipe"
    recipe_id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(50))
    label: Mapped[str] = mapped_column(String(50))
    products: Mapped[List['Product_Recipe']] = relationship(back_populates='recipe',cascade='delete')

def execute_select_product_qty_per_restaurant(label: str):
    result = {}
    result['stocks'] = list()
    with Session(engine) as session:
        try:
            result['product'] = session.scalars(
                select(Product)
                .where(Product.label == label)
            ).one()
        except NoResultFound:
            return None
        try:
            restaurants = session.scalars(
                select(Restaurant)
            ).all()
        except NoResultFound:
            return None
        for restaurant in restaurants:
            try:
                result['stocks'].append(session.scalars(
                    select(Stock)
                    .where(Stock.product_id == result['product'].product_id)
                    .where(Stock.restaurant_id == restaurant.restaurant_id)
                    .order_by(Stock.inventory_date.desc())
                ).first())

            except NoResultFound:
                result.append(Stock(reetaurant=restaurant, product=result['product'], qt=0))
        return result


def execute_insert_product(**kwargs):
    with Session(engine) as session:
        product = Product(**kwargs)
        session.add(product)
        try:
            session.commit()
            session.refresh(product)
        except IntegrityError:
            session.rollback()
            product = session.scalars(
                select(Product).where(Product.label == kwargs['label'])
            ).one()
        return product

def execute_update_stock(**kwargs):
    with Session(engine) as session:
        product = session.scalars(
            select(Product).where(Product.product_id ==  kwargs['product_id'])
        ).one()
        restaurant = session.scalars(
            select(Restaurant).where(Restaurant.restaurant_id == kwargs['restaurant_id'])
        ).one()

        try:
            product_restaurant = session.scalars(
                select(Stock)
                .options(selectinload(Stock.restaurant), selectinload(Stock.product))
                .where(Stock.restaurant_id == kwargs['restaurant_id'])
                .where(Stock.product_id == kwargs['product_id'])
            ).one()
        except NoResultFound:
            if kwargs['qt'] > 0:
                product_restaurant = Stock(restaurant= restaurant, product=product, qt= kwargs['qt'], inventory_date=datetime.date.today())
                session.add(product_restaurant)
                session.commit()
                session.refresh(product_restaurant)
            else:
                raise ValueError
        if (result:=product_restaurant.qt + kwargs['qt']) > 0:
            product_restaurant.qt = result
            product_restaurant.inventory_date = datetime.date.today()
            session.commit()
            session.refresh(product_restaurant)
        else:
            raise ValueError
        return product_restaurant

def execute_insert_recipe(**kwargs):
    kwargs['label'] = src.api.util.normalize_str(kwargs['name'])
    recipe = Recipe(name = kwargs['name'], label=kwargs['label'])
    with Session(engine) as session:
        for product_from_query in kwargs['products']:
            product_recipe = src.api.bdd_model.Product_Recipe(qt = product_from_query['qt'])
            product_from_query['product']['label'] = src.api.util.normalize_str(product_from_query['product']['name'])
            for product in recipe.products:
                if product.label == product_from_query['product']['label']:
                    break
            else:
                try:
                    product = session.scalars(
                        select(Product)
                        .where(Product.label == product_from_query['product']['label'])
                    ).one()
                except NoResultFound:
                    product = Product(name=product_from_query['product']['name'],
                                      label=product_from_query['product']['label'])
            product_recipe.product=product
            session.add(product_recipe)
            recipe.products.append(product_recipe)
        session.add(recipe)
        session.commit()
        session.refresh(recipe)
        for product in recipe.products:
            print(product.__dict__)

        return recipe

def execute_select_recipe_by_name(label:str):
    with Session(engine) as session:
        try:
            return session.scalars(
                select(Recipe)
                .options(selectinload(Recipe.products).selectinload(Product_Recipe.product))
                .where(Recipe.label == label)
            ).one()

        except NoResultFound:
            return None

def execute_select_recipe_qt_by_restaurant(name:str):
    label = src.api.util.normalize_str(name)
    with Session(engine) as session:

        try:
            recipe = session.scalars(
                select(Recipe)
                .options(selectinload(Recipe.products).selectinload(Product_Recipe.product))
                .where(Recipe.label == label)
            ).one()
        except NoResultFound:
            return None
        result = {'recipe': serialize(recipe), 'qt':[]}
        restaurants = session.scalars(
            select(Restaurant).options(selectinload(Restaurant.products).selectinload(Stock.product))
        ).all()
        for restaurant in restaurants:
            l = []
            for product in recipe.products:
                for product_r in restaurant.products:
                    if product_r.product_id == product.product_id:
                        l.append(product_r.qt // product.qt)
                        break
                else:
                    l.append(0)
            result['qt'].append({'restaurant':serialize(restaurant), 'qt':min(l)})
        return result
def serialize(obj: Base) -> dict:
    result = {}
    attributes = vars(obj)
    for k, v in attributes.items():
        if isinstance(v, Base):
            result[k] = serialize(v)
        elif isinstance(v, list):
            result[k] = list()
            for item in v:
                if isinstance(item, Base):
                    result[k].append(serialize(item))
                else:
                    result[k].append(item)
        else:
            result[k] = v
    return result