from src.api import util
import pandas as pd


df = pd.read_csv('../data/extract_B.csv')
df['date'] = pd.to_datetime(df['date'], format="%d/%m/%Y")
df['stock'] = df['stock'].str.lower().str.strip()
df['label'] = df['stock'].apply(util.normalize_str)
df.rename(columns={'stock':'name', 'inventory':'qt'}, inplace=True)

print(df)

util.dataframe_to_sql(df)

