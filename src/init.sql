DROP DATABASE IF EXISTS ecorp_si;
CREATE DATABASE ecorp_si;
GRANT ALL PRIVILEGES ON ecorp_si.* TO 'npo'@'localhost';
use ecorp_si;

CREATE TABLE product(
    product_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(50) NOT NULL,
    label VARCHAR(50) NOT NULL,
    UNIQUE(label),
    UNIQUE(name)
);

CREATE TABLE restaurant(
    restaurant_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL
);

CREATE TABLE product_restaurant(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    product_id INT NOT NULL REFERENCES product.product_id,
    restaurant_id INT NOT NULL REFERENCES restaurant.restaurant_id,
    qt INT NOT NULL,
    inventory_date DATE,
    UNIQUE(product_id,restaurant_id,inventory_date)
);
