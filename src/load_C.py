import pandas as pd
from src.api import util

df = pd.read_csv('../data/extract_C.csv', header=None)
df.rename(columns={0: 'name', 1:'qt'}, inplace=True)
df['name'] = df['name'].str.lower().str.strip()
df['label'] = df['name'].apply(util.normalize_str)
df = df.groupby('label').agg({'name': lambda x: set(x.values.tolist()), 'qt': 'sum'})
df['date'] = pd.to_datetime('today').normalize()
df.loc[df['qt'] < 0, ['qt']] = 0
df.reset_index(inplace=True)
print(df)
util.dataframe_to_sql(df)