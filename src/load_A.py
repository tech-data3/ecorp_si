from src.api import util

import pandas as pd

df = pd.read_csv('../data/extract_A.csv')
df['product'] = df['product'].str.lower().str.strip()
df['label'] = df['product'].apply(util.normalize_str)
df = df.groupby('label').agg({'product': lambda x: set(x.values.tolist()), 'stock': 'sum'})
df.rename(columns={'product': 'name', 'stock':'qt'}, inplace=True)
df['date'] = pd.to_datetime('today').normalize()
df.reset_index(inplace=True)

util.dataframe_to_sql(df)
